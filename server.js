const http = require('http')
const app = require('./app')
const PORT = process.env.PORT || 3000
const SocketService = require('./socketService')

const server = http.createServer(app)
app.set("SocketService", new SocketService(server));

server.listen(PORT, () => {
    console.log("listen on http://localhost:" + PORT)
})
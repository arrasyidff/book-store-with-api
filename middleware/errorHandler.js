module.exports = (err, req, res, next) => {
    // next(err))
    const { name, line = null } = err
    switch (name) {
        case "SequelizeUniqueConstraintError":
            res.status(400).json({msg: err.errors[0].message})
        break;
        case "SequelizeValidationError":
            res.status(400).json({msg: err.errors[0].message})
            break;
        case "invalid_account":
            res.status(401).json({msg: "invalid account"})
        break;
        case "authenticate":
            res.status(401).json({msg: "login first"})
        break;
        case "authorize":
            res.status(401).json({msg: "you are not authorize"})
        break;
        case "not_found":
            res.status(404).json({msg: "data not found", line})
        break;
        case "out_of_stock":
            res.status(400).json({msg: "Sorry, out of stock"})
        break;
        case "maximum_stock":
            res.status(400).json({msg: "can't exceed stock limit"})
        break;
        case "minimum_quantity":
            res.status(400).json({msg: "cannot be less than 0"})
        break;
        case "not_foud_cart":
            res.status(400).json({msg: "there are no items in your cart"})
        break;
        case "user_doesn't_have_address":
            res.status(400).json({msg: "user does not have address"})
        break;
        case "not_found_1_or_all_book":
            res.status(400).json({msg: 'one or the book you bought is missing'})
        break;
        case "not_found_1_or_all_book_shipment":
            res.status(400).json({msg: 'one or more book in shipment not found'})
        break;
        case "not_found_address":
            res.status(400).json({msg: 'address not found'})
        break;
        case "error midtrans":
            res.status(400).json({msg: 'error midtrans'})
        break;
        default:
            console.log(err)
            res.status(500).json({msg: 'internal service error'})
            break;
    }
}
const { Admin, Users } = require("../models");

class Authorization {
  static async admin(req, res, next) {
    try {
      const { id, email, role } = req.logInUser;
      let findAdmin;
      if (role && role === "admin") {
        findAdmin = await Admin.findOne({ where: { email } });
        if (findAdmin) {
          next();
        } else {
          throw { name: "authorize" };
        }
      } else {
        throw { name: "authorize" };
      }
    } catch (err) {
      next(err);
    }
  }
  static async user(req, res, next) {
    try {
      const { id, email } = req.logInUser;
      let findUser;
      findUser = await Users.findOne({ where: { id } });
      if (findUser) {
        if (findUser.role) {
          throw { name: "authorize" };
        } else {
          next();
        }
      } else {
        throw { name: "authorize" };
      }
    } catch (err) {
      next(err);
    }
  }
}

module.exports = Authorization;

// module.exports = async (req, res, next) => {
//     try {
//         const {id, email, role} = req.logInUser
//         let findAdmin
//         if (role && role === "admin") {
//             findAdmin = await Admin.findOne({ where: {id} })
//             if (findAdmin) {
//                 next()
//             } else {
//                 throw{name: "authorize"}
//             }
//         } else {
//             throw{name: "authorize"}
//         }

//     } catch (err) {
//         next(err)
//     }
// }

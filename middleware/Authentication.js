const { Admin, Users } = require("../models")
const { compareToken } = require("../helper/generateToken")

module.exports = async (req, res, next) => {
    try {
        const { access_token } = req.headers
        if (!access_token) {
            next({ name: "authenticate" })
        } else {
            const decoded = compareToken(access_token)
            req.logInUser = decoded
            if (decoded.role) {
                const findAdmin = await Admin.findOne( {where : {id: decoded.id}} )
                if (findAdmin) {
                    next()
                } else {
                    throw{name: "authenticate"}
                }

            } else {
                const findUser = await Users.findOne( {where : {id: decoded.id}} )
                if (findUser) {
                    next()
                } else {
                    throw{name: "authenticate"}
                }
            }
        }
    } catch (err) {
        next(err)
    }
}
const socketIo = require("socket.io");

class SocketService {
  constructor(server) {
    this.io = socketIo(server)
    this.socket = null
    this.io.on('connection', (socket) => {
      this.socket = socket
      console.log(socket.id, 'user connected')
      this.socket.on('private-message', (message) => {
        console.log(message, 'test')
      })
    })
  }

  emitter(event, body) {
    if (body) {
      // this.socket.emit(event, body) //basic event
      // this.io.emit(event, body) // to all
      // this.socket.broadcast.emit(event, body) // to all client
      // this.socket.to().emit(event, body) // to all client
      switch (event) {
        case 'refreshPageAfterTransaction':
          this.socket.broadcast.emit(event, body) // to all client
          break;
        default:
          break;
      }
    }
  }
}

module.exports = SocketService

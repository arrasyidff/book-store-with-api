const express = require("express")
const app = express()
const cors = require('cors')
const path = require('path')

const routes = require("./routes")
const errorHandler = require("./middleware/errorHandler")

app.use(cors()) // include before other routes

app.use(express.urlencoded({ extended: false}))
app.use(express.json())
app.use(express.static('public'))
app.use("/uploads", express.static(path.join(__dirname, "uploads")))

app.use("/", routes)
app.use(errorHandler)

module.exports = app
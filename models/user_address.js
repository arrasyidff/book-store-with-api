'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_Address extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_Address.belongsTo(models.Users, {
        foreignKey: "user_id",
        targetKey: "id"
      })
      User_Address.hasMany(models.Order, {
        foreignKey: "user_address_id",
        targetKey: "id"
      })
    }
  };
  User_Address.init({
    province: DataTypes.STRING,
    city: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    receiver: DataTypes.STRING,
    postal_code: DataTypes.STRING,
    detail_address: DataTypes.STRING,
    province_id: DataTypes.INTEGER,
    city_id: DataTypes.INTEGER,
    is_main_address: DataTypes.BOOLEAN,
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User_Address',
  });
  return User_Address;
};
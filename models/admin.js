'use strict';
const {
  Model
} = require('sequelize');

const { hashingPass } = require("../helper/generatePassword")
module.exports = (sequelize, DataTypes) => {
  class Admin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Admin.init({
    email: {
      type: DataTypes.STRING,
      validate: {
        validateEmptyChar(value) {
          if (!value) {
            throw new Error("email can't be empty")
          }
        },
        isEmail: {
          msg: "email must be email format"
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        validateEmptyChar(value) {
          if (!value) {
            throw new Error("password can't be empty")
          }
        },
        validateMinChar(value) {
          if (value.length < 5) {
            throw new Error("password min 5 character")
          }
        }
      }
    },
    role: {
      type: DataTypes.STRING,
    }
  }, {
    hooks: {
      beforeCreate(user, opts) {
        if (!user.role) {
          user.role = "admin"
        }
        user.password = hashingPass(user.password)
      }
    },
    sequelize,
    modelName: 'Admin',
  });
  return Admin;
};
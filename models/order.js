'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Order.hasMany(models.Order_item, {
        foreignKey: "order_id",
        sourceKey: "id"
      })
      Order.belongsTo(models.User_Address, {
        foreignKey: "user_address_id",
        targetKey: "id"
      })
    }
  };
  Order.init({
    user_id: DataTypes.INTEGER,
    total_price: DataTypes.INTEGER,
    invoice: DataTypes.STRING,
    transaction_id: DataTypes.STRING,
    order_status: DataTypes.STRING,
    courier_type: DataTypes.STRING,
    cost: DataTypes.INTEGER,
    user_address_id: DataTypes.INTEGER,
    bill_key: DataTypes.STRING,
    biller_code: DataTypes.STRING,
    payment_method: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Order',
  });
  return Order;
};
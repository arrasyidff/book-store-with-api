'use strict';
const {
  Model
} = require('sequelize');

const { hashingPass } = require("../helper/generatePassword")
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Users.hasMany(models.User_Address, {
        foreignKey: "user_id",
        sourceKey: "id"
      })
      Users.hasMany(models.Wishlist, {
        foreignKey: "user_id",
        sourceKey: "id"
      })
      Users.hasMany(models.Cart, {
        foreignKey: "user_id",
        sourceKey: "id"
      })
      Users.hasMany(models.Shipment, {
        foreignKey: "user_id",
        sourceKey: "id"
      })
    }
  };
  Users.init(
  {
    full_name: {
      type: DataTypes.STRING,
      validate: {
        validateEmptyChart(value) {
          if (!value) {
            throw new Error("first name can't be empty")
          }
        },
      }
    },
    date_of_birth: {
      type: DataTypes.DATEONLY
    },
    gender: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        validateEmptyChar(value) {
          if (!value) {
            throw new Error("email can't be empty")
          }
        },
        isEmail: {
          msg: "email must be email format"
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        validateEmptyChar(value) {
          if (!value) {
            throw new Error("password can't be empty")
          }
        },
        validateMinChar(value) {
          if (value.length < 5) {
            throw new Error("password min 5 character")
          }
        }
      }
    },
    is_google: {
      type: DataTypes.BOOLEAN
    },
    phone_number: {
      type: DataTypes.STRING
    },
    image: {
      type: DataTypes.STRING
    },
  },
  {
    hooks: {
      beforeCreate(user, opts) {
        if (!user.is_google) {
          user.password = hashingPass(user.password)
        }
      }
    },
    sequelize,
    modelName: 'Users',
  });
  return Users;
};
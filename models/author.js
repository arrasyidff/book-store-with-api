'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Author extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Author.hasMany(models.Books, {
        foreignKey: "author_id",
        sourceKey: "id"
      })
    }
  };
  Author.init({
    full_name: DataTypes.STRING,
    date_of_birth: DataTypes.DATEONLY,
    gender: DataTypes.STRING,
    email: DataTypes.STRING,
    phone_number: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Author',
  });
  return Author;
};
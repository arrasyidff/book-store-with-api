'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Books extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Books.belongsTo(models.Category, {
        foreignKey: "category_id",
        targetKey: "id"
      })
      Books.belongsTo(models.Author, {
        foreignKey: "author_id",
        targetKey: "id"
      })
      Books.belongsTo(models.Publisher, {
        foreignKey: "publisher_id",
        targetKey: "id"
      })
      Books.hasMany(models.Wishlist, {
        foreignKey: "book_id",
        sourceKey: "id"
      })
      Books.hasMany(models.Cart, {
        foreignKey: "book_id",
        sourceKey: "id"
      })
      Books.hasMany(models.Shipment, {
        foreignKey: "book_id",
        sourceKey: "id"
      })
      Books.hasMany(models.Order_item, {
        foreignKey: 'book_id',
        sourceKey: 'id'
      })
    }
  };
  Books.init({
    title: {
      type: DataTypes.STRING,
      // validate: {
      //   notEmpty: {
      //     args: true,
      //     msg: "title can't be empty"
      //   }
      // }
    },
    author_id: {
      type: DataTypes.INTEGER,
    },
    isbn_number: {
      type: DataTypes.STRING,
    },
    publisher_id: {
      type: DataTypes.INTEGER,
    },
    date_of_publish: {
      type: DataTypes.DATEONLY,
    },
    number_of_pages: {
      type: DataTypes.INTEGER,
    },
    category_id: {
      type: DataTypes.INTEGER,
    },
    price: {
      type: DataTypes.INTEGER,
    },
    stock: {
      type: DataTypes.INTEGER,
    },
    recommendation: {
      type: DataTypes.INTEGER,
    },
    sold: {
      type: DataTypes.INTEGER,
    },
    cover: {
      type: DataTypes.STRING,
    },
  }, {
    sequelize,
    modelName: 'Books',
  });
  return Books;
};
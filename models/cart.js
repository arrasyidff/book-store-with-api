'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Cart extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Cart.belongsTo(models.Books, {
        foreignKey: "book_id",
        targetKey: "id"
      })
      Cart.belongsTo(models.Users, {
        foreignKey: "user_id",
        targetKey: "id"
      })
    }
  };
  Cart.init({
    book_id: DataTypes.INTEGER,
    quantity: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    is_selected: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Cart',
  });
  return Cart;
};
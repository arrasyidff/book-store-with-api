"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Categories",
      [
        {
          name: "Pengembangan Diri",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Ilmu Sosial",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Sosial Budaya",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Cerita Anak",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Ekonomi",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Kewirausahaan",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Orang Tua dan Anak",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Islam",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Kebudayaan",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Romance",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Busana Tradisional",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Fantasi",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Sastra",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Bisnis Investasi",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Comic",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Drama",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Categories", null, {});
  },
};

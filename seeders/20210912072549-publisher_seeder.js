"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert(
      "Publishers",
      [
        {
          name: "Psikologi Corner",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Pustaka Jawi",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "PT Rumah Tulis",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Play Round",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Elex Media Komputindo",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Bee Media",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Buku Aquarius",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Anak Hebat Indonesia",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Andi Publisher",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Gema Insani",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Diandra Primamitra",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Fathan Prima Media",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Gramedia Pustaka Utama",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Kompas",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "M&C",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Kpg",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "TDW",
          address: null,
          email: null,
          phone_number: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Publishers', null, {});
  },
};

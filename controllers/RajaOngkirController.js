const axios = require('axios');

axios.defaults.baseURL = 'https://api.rajaongkir.com/starter'
axios.defaults.headers.common['key'] = '465d2374ebaf0e63f62e857b7e510874'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

class RajaOngkirController {
  static getSuccessResponse (data = [], msg = '') {
		return {
		  status: 'success',
		  data: data,
		  msg: msg
		}
	}

  static getProvince (req, res) {
    let url = '/province'
    if (req.query.id) {
      url += `?id=${req.query.id}`
    }
    axios({
      url,
      method: 'GET'
    })
    .then(response => {
      let copyData = JSON.parse(JSON.stringify(response.data.rajaongkir.results))
      const result = copyData.map(el => {
        return { value: el.province_id, label: el.province }
      })
      res.status(200).json(RajaOngkirController.getSuccessResponse(result, 'success fetch provinces'))
    })
    .catch(err => {
      res.json(err)
    })
  }

  static getCity (req, res) {
    let url = '/city'
    if (req.query.province) {
      url += `?province=${req.query.province}`
    } 
    axios({
      url,
      method: 'GET'
    })
    .then(response => {
      let copyData = JSON.parse(JSON.stringify(response.data.rajaongkir.results))
      const result = copyData.map(city => {
        return {
          value: city.city_id,
          label: city.city_name,
          postal_code: city.postal_code
        }
      })
      res.status(200).json(RajaOngkirController.getSuccessResponse(result, 'success fetch cities'))
    })
    .catch(err => {
      res.json(err)
    })
  }

  static getCost (req, res) {
    try {
      const {origin, destination, weight, courier} = req.body
      axios({
        url: '/cost',
        method: 'POST',
        data: {
          origin,
          destination,
          weight,
          courier
        }
      })
        .then(response => {
          const result = JSON.parse(JSON.stringify(response.data.rajaongkir.results))
          result.forEach(element => {
            element.costs.forEach(courier => {
              courier.cost.forEach((cost, i) => {
                cost.id = element.code + courier.service + (i+1)
              })
            })
          });
          res.json(result)
        })
        .catch(err => {
          res.json('error bro')
        })
    } catch (error) {
      res.json(error)
    }
  }
}

module.exports = RajaOngkirController
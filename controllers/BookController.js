const { Books, Publisher, Author, Category, Wishlist, Cart, } = require("../models");
const multer = require("multer");
const { storage, fileFilter } = require("../helper/upload");
const { Op } = require("sequelize");
let host = "http://localhost:3000/";
if (process.env.NODE_ENV === "production") {
  host = "https://book-store-arfafa.herokuapp.com/";
}
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5, //5mb
  },
  fileFilter: fileFilter,
}).single("cover");

const BookService = require('../services/BookService');
const WishlistService = require("../services/WishlistService");
const CartService = require("../services/CartService");

class BookController {
  static getSuccessResponse (data = [], msg = '') {
    return {
      status: 'success',
      data: data,
      msg: msg
    }
  }

  static async fetchNameBook(req, res, next) {
    try {
      const bookService = new BookService()
      const result = await bookService.getNamesBook(req.query.title)
      let finalResult = result.map((el) => {
        return { id: el.id, value: el.title };
      });
      res.status(200).json(BookController.getSuccessResponse(finalResult, 'managed to get data'));
    } catch (error) {
      res.json(error);
    }
  }

  static async getAllBook(req, res, next) {
    const limit = req.query.limit ? req.query.limit : 5
    const offset = req.query.page ? (req.query.page - 1) * limit : 0 * limit
    let result = []
    let total_data = 0
    let total_page = 0
    let current_page = req.query.page ? +req.query.page : 1
    if (req.query.category) {
      let { total, books, pages } = await new BookService().getBooksByCategory(req.query.category, limit, offset)
      total_data = total
      total_page = pages
      result = books
    } else if (req.query.author) {
      let { total, books, pages } = await new BookService().getBooksByAuthor(req.query.author, limit, offset)
      total_data = total
      total_page = pages
      result = books
    } else if (req.query.publisher) {
      let { total, books, pages } = await new BookService().getBooksByPublisher(req.query.publisher, limit, offset)
      total_data = total
      total_page = pages
      result = books
    } else if (req.query.title) {
      let { total, books, pages } = await new BookService().getBooksByTitle(req.query.title, limit, offset)
      total_data = total
      total_page = pages
      result = books
    } else {
      let { total, books, pages } = await new BookService().getAllBooks(null, limit, offset)
      total_data = total
      total_page = pages
      result = books
    }

    let wishlistByUserBooksId = []
    if (req.query.user_id) {
      const booksId = result.map(el => el.id)
      const userWishlist = await new WishlistService().getWishlistByBookUserId(
        req.query.user_id,
        booksId
      )
      wishlistByUserBooksId = userWishlist.map(el => el.book_id)
    }

    const response = JSON.parse(JSON.stringify(result))
    response.forEach((book, i) => {
      if (wishlistByUserBooksId.includes(book.id)) {
        response[i].is_wishlist = true
      } else {
        response[i].is_wishlist = false
      }
    });
    
    res.json(BookController.getSuccessResponse({ total_data, total_page: total_page, current_page, data: response }, 'managed to get data'))
  }

  static async findBook(req, res, next) {
    try {
      const { id } = req.params;
      const findBook = await new BookService().getBookById(id)
      const payload = { book: JSON.parse(JSON.stringify(findBook)) };

      if (!findBook) {
        throw { name: "not_found" };
      }

      if (req.query.user_id) {
        const findCart = await new CartService().getCartByBookUserId(+req.query.user_id, +id)
        payload.book['Cart'] = findCart
        const findWishlist = await new WishlistService().getWishlistByBookUserId(+req.query.user_id, +id)
        payload.book.is_wishlist = findWishlist.length > 0 ? true : false
      }

      res.status(200).json(BookController.getSuccessResponse(payload.book, 'success get book'));
    } catch (error) {
      next(error);
    }
  }

  static async create(req, res, next) {
    upload(req, res, async (err) => {
      if (err instanceof multer.MulterError) {
        // A Multer error occurred when uploading.
        // Terjadi kesalahan Multer saat mengunggah.
        // seperti size terlalu besar
        res.status(400).json(err);
      } else if (err) {
        // An unknown error occurred when uploading.
        // error tidak diketahui saat mengunggah
        // seperti extension tidak valid
        res.status(400).json(err.msg);
      }
      try {
        const {
          title,
          author_id,
          isbn_number,
          publisher_id,
          date_of_publish,
          number_of_pages,
          category_id,
          price,
          stock,
          recommendation,
          sold,
          cover,
        } = req.body;
        const createBook = await Books.create({
          title,
          author_id,
          isbn_number,
          publisher_id,
          date_of_publish,
          number_of_pages,
          category_id,
          price,
          stock,
          recommendation,
          sold,
          cover: "http://localhost:3000/" + req.file.path,
        });
        res.json(createBook);
      } catch (error) {
        console.log(error);
        next(err);
      }
    });
  }

  static async update(req, res, next) {
    upload(req, res, async (err) => {
      if (err instanceof multer.MulterError) {
        // A Multer error occurred when uploading.
        // Terjadi kesalahan Multer saat mengunggah.
        // seperti size terlalu besar
        res.status(400).json(err);
      } else if (err) {
        // An unknown error occurred when uploading.
        // error tidak diketahui saat mengunggah
        // seperti extension tidak valid
        res.status(400).json(err.msg);
      }
      try {
        const { id } = req.params;
        res.json(req.body)
        const {
          title,
          author_id,
          isbn_number,
          publisher_id,
          date_of_publish,
          number_of_pages,
          category_id,
          price,
          stock,
          recommendation,
          sold,
          cover,
        } = req.body;
        const updateBook = await Books.update(
          {
            title,
            author_id,
            isbn_number,
            publisher_id,
            date_of_publish,
            number_of_pages,
            category_id,
            price,
            stock,
            recommendation,
            sold,
            cover: host + req.file.path,
          },
          { where: { id }, returning: true }
        );
        if (updateBook[1][0]) {
          res.status(200).json({ response: updateBook[1][0] });
        } else {
          next({ name: "not_found" });
        }
      } catch (error) {
        next(err);
      }
    });
  }

  static async delete(req, res, next) {
    try {
      const { id } = req.params;
      const deleteBook = await Books.destroy({ where: { id } });

      if (deleteBook) {
        res.status(200).json({ response: "book success deleted" });
      } else {
        next({ name: "not_found" });
      }
    } catch (err) {
      next(err);
    }
  }
}

module.exports = BookController;

const { Publisher, Books } = require("../models")
const { Op } = require("sequelize")

class PublisherController {
  static async getAllPublisher (req, res, next) {
    try {
      const { page, publisher } = req.query
      let current_page = page ? page - 1 : 0
      let payload = {
        include: [
          { 
            model: Books
          }
        ],
        offset: current_page * 12,
        limit: 12
      }
      let where
      if (publisher) {
        where = {
          name: {
            [Op.iLike]: `%${publisher}%`
          }
        }
        payload.where = where
      }
      const total_publisher = await Publisher.count({where})
      const result = await Publisher.findAll(payload)
      const total_page = Math.ceil(total_publisher / 12)
      res.status(200).json({total_data: total_publisher, total_page, current_page, response: result})
    } catch (error) {
      next(error)
    }
  }
}

module.exports = PublisherController
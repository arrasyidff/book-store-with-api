const { Author, Books } = require("../models");
const { Op } = require("sequelize")

class AuthorController {
  static async getAllAuthor (req, res, next) {
    try {
      const { page, author } = req.query
      
      let current_page = page ? page - 1 : 0
      let payload = {
        include: [
          {
            model: Books
          }
        ],
        offset: current_page * 12,
        limit: 12
      }
      let where
      if (author) {
        where = {
          full_name: {
            [Op.iLike]: `%${author}%`
          }
        }
        payload.where = where
      } 
      const total_author = await Author.count({where})
      const result = await Author.findAll(payload)
      const total_page = Math.ceil(total_author / 12)
      res.status(200).json({total_data: total_author, total_page, current_page, response: result})
    } catch (error) {
      next(error)
    }
  }

  static async getAuthorsName (req, res, next) {
    try {
      const {limit = 12, author} = req.query
      const payload = {
        attributes: ["id", "full_name"],
        limit
      }
      if (limit === 'all') {
        delete payload.limit
      }
      if (author) {
        payload.where = {
          full_name: {
            [Op.iLike]: `%${author}%`
          }
        }
      }
      let authorsName = await Author.findAll(payload)
      authorsName = authorsName.map(el => {
        return {id: el.id, name: el.full_name}
      })
      res.status(200).json({limit, response: authorsName})
    } catch (error) {
      next(error)
    }
  }
}

module.exports = AuthorController;

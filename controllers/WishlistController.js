const BookService = require("../services/BookService");
const WishlistService = require("../services/WishlistService");

class WishlistController {
  static getSuccessResponse (data = [], msg = '') {
    return {
      status: 'success',
      data: data,
      msg: msg
    }
  }

  static async getAllWishList(req, res, next) {
    try {
      const { key } = req.query
      const { id } = req.logInUser
      const wishlistService = new WishlistService()
      if (!key) {
        const result = await wishlistService.getAllWishlist(id)
        const response = JSON.parse(JSON.stringify(result))
        response.forEach((item, i) => {
          response[i].is_wishlist = true
        });
        res.status(200).json(WishlistController.getSuccessResponse(response, 'managed to get data'))
      } else if (key === 'count') {
        const getTotalWishlist = await wishlistService.getTotalWishlist(id)
        res.status(200).json(WishlistController.getSuccessResponse({total_data: getTotalWishlist}, 'managed to get total data'))
      }
    } catch (error) {
      next(error)
    }
  }

  static async create(req, res, next) {
    try {
      const { book_id } = req.body;
      const user_id = +req.logInUser.id

      const bookService = new BookService()
      const book = await bookService.getBookById(book_id)

      if (!book) {
        throw { name: 'not_found' }
      }

      const wishlistService = new WishlistService()
      const findWishlist = await wishlistService.getWishlistByBookId(user_id, book_id);
      console.log(findWishlist, book_id)
      if (findWishlist) {
        const deleteWishlist = await wishlistService.deleteWishlist(user_id, book_id);
        res.status(200).json(WishlistController.getSuccessResponse(deleteWishlist, "wishlist deleted"));
      } else {
        const createWishlist = await wishlistService.addWishlist(user_id, book_id);
        res.status(200).json(WishlistController.getSuccessResponse(createWishlist, "wishlist created"));
      }
    } catch (error) {
      next(error);
    }
  }
}

module.exports = WishlistController;

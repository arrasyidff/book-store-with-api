const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client('526852320651-25tkreshg4sn3dv6pthivfpqm8dh83tj.apps.googleusercontent.com');
const { Users, User_Address } = require("../models");
const { comparePass } = require("../helper/generatePassword");
const { generateToken } = require("../helper/generateToken");
const { storage, fileFilter } = require("../helper/upload");
let host = "http://localhost:3000/"
if (process.env.NODE_ENV === "production") {
  host = "https://book-store-arfafa.herokuapp.com/"
}
const multer = require("multer");
const UserService = require("../services/UserService");
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5, //5mb
  },
  fileFilter: fileFilter,
}).single("image");

class UserController {
  static getSuccessResponse (data = [], msg = '') {
    return {
      status: 'success',
      data: data,
      msg: msg
    }
  }

  static async getAllUser(req, res, next) {
    try {
      const getAll = await Users.findAll({
        attributes: { exclude: ["password", "createdAt", "updatedAt"] },
        include: [
          {
            model: User_Address,
            attributes: { exclude: ["createdAt", "updatedAt"] },
          },
        ],
      });
      res.json(getAll);
    } catch (error) {
      next(error);
    }
  }

  static async findOneUser(req, res, next) {
    try {
      const userService = new UserService()
      const { id } = req.params;
      const getUser = await userService.getUser(id)
      if (getUser) {
        res.json(UserController.getSuccessResponse(getUser, 'success get user'));
      } else {
        throw { name: "not_found" };
      }
    } catch (error) {
      next(error);
    }
  }

  static async updateUser(req, res, next) {
    try {
      const { id } = req.params
      const { full_name, date_of_birth, gender, email, phone_number } = req.body
      
      const userService = new UserService()
      const findUser = await userService.getUser(id)
      if (!findUser) {
        throw {name: 'not_found'}
      }

      const update = await userService.update(id, { full_name, date_of_birth, gender, email, phone_number })
      res.json(UserController.getSuccessResponse(update[1][0], 'success update user'))
    } catch (error) {
      next(error)
    }
  }

  static async changePhoto(req, res, next) {
    try {
      const { id } = req.params;
      const findUser = await Users.findOne({
        where: {
          id,
        },
      });
      if (!findUser) {
        throw { name: "not_found", line: 89 };
      } else {
        upload(req, res, async (err) => {
          if (err instanceof multer.MulterError) {
            // A Multer error occurred when uploading.
            // Terjadi kesalahan Multer saat mengunggah.
            // seperti size terlalu besar
            res.status(400).json(err);
          } else if (err) {
            // An unknown error occurred when uploading.
            // error tidak diketahui saat mengunggah
            // seperti extension tidak valid
            res.status(400).json({message: err.message});
          }
          try {
            const updateUser = await Users.update(
              {
                image: host + req.file.path,
              },
              {
                where: {
                  id: findUser.id,
                },
                returning: true,
              }
            );  
            res.status(200).json(UserController.getSuccessResponse(updateUser[1][0], 'success update image profile'));
          } catch (error) {
            next(error);
          }
        });
      }
    } catch (error) {
      next(error)
    }
  }

  static async register(req, res, next) {
    try {
      const { full_name, email, password } = req.body;
      let createUser = await Users.create({ full_name, email, password });
      res.status(201).json({
        response: {
          full_name: createUser.full_name,
          email: createUser.email,
        },
      });
    } catch (err) {
      next(err);
    }
  }

  static async login(req, res, next) {
    try {
      const { email, password } = req.body;
      const findUser = await Users.findOne({ where: { email } });
      if (!findUser) {
        next({ name: "invalid_account" });
      } else {
        if (comparePass(password, findUser.password)) {
          const access_token = generateToken({
            id: findUser.id,
            email: findUser.email,
          });
          res
            .status(200)
            .json({
              access_token,
              id: findUser.id,
              email: findUser.email,
              full_name: findUser.full_name,
            });
        } else {
          next({ name: "invalid_account" });
        }
      }
    } catch (err) {
      next(err);
    }
  }

  static async googleLogin (req, res, next) {
    try {
      const userService = new UserService()
      const { token } = req.body
      const ticket = await client.verifyIdToken({
        idToken: token,
        audience: '526852320651-25tkreshg4sn3dv6pthivfpqm8dh83tj.apps.googleusercontent.com',  // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
      })
      const { name, picture, email } = ticket.getPayload()
      let user = await userService.getUserByEmail(email)
      let access_token = null
      if (user) {
        access_token = generateToken({
          id: user.id,
          email: user.email,
        });
      } else {
        let createUser = await Users.create({ full_name: name, email, image: picture, is_google: true })
        access_token = generateToken({
          id: createUser.id,
          email: createUser.email,
        });
        user = createUser
      }
      res.json({
        access_token,
        id: user.id,
        email: user.email,
        full_name: user.full_name,
      })
    } catch (error) {
      next(error)
    }

  }
}

module.exports = UserController;

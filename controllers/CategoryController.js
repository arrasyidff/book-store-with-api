const { Category } = require("../models");
const { Op } = require("sequelize")

class CategoryController {
  static async getAllCategory(req, res, next) {
    try {
      const result = await Category.findAll({
        order: [['name', 'ASC']]
      })
      res.status(200).json({response: result})
    } catch (error) {
      res.json(error)
    }
  }

  static async getCategoriesName(req, res, next) {
    try {
      const { limit = 12, category } = req.query
      const payload = {
        attributes: ["id", "name"],
        limit
      }
      if (limit === 'all') {
        delete payload.limit
      }
      if (category) {
        payload.where = {
          name: {
            [Op.iLike]: `%${category}%`
          }
        }
      }
      const categoriesName = await Category.findAll(payload)
      res.status(200).json({limit, response: categoriesName})
    } catch (error) {
      next(error)
    }
  }
}

module.exports = CategoryController;

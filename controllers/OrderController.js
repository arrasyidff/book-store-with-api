const { Cart, Books, Order, Order_item, sequelize, User_Address } = require("../models");
const { Op } = require("sequelize")
const BookService = require("../services/BookService");
const ShipmentService = require("../services/ShipmentService");
const OrderService = require("../services/OrderServices");

class OrderController {
  static getSuccessResponse (data = [], msg = '') {
		return {
		  status: 'success',
		  data: data,
		  msg: msg
		}
	}

  static async checkout(req, res, next) {
    try {
      const { id } = req.logInUser
      const {payment_method, total_price, courier, cost, user_address_id, books} = req.body
      let booksId = books.map(el => {
        return el.book_id
      })

      const bookService = new BookService()
      const shipmentService = new ShipmentService()
      const findBooks = await bookService.getBooksByIds(booksId)
      if (findBooks.books.length !== books.length) {
        throw {name: 'not_found_1_or_all_book'}
      }

      const findShipments = await shipmentService.getAllShipmentsByUserId(id)
      if (findShipments.length !== books.length) {
        throw {name: 'not_found_1_or_all_book_shipment'}
      }

      const orderService = new OrderService()
      const checkout = await orderService.checkout(req.logInUser.id, payment_method, total_price, courier, cost, user_address_id, books)
      res.status(201).json(OrderController.getSuccessResponse(checkout[1][0], 'successfully create order'))
    } catch (error) {
      next(error);
    }
  }

  static async updateOrderAfterPay(req, res, next) {
    try {
      const {transaction_time, transaction_status, transaction_id, status_message,
        status_code, signature_key, payment_type, order_id, merchant_id, gross_amount,
        fraud_status, currency, biller_code, bill_key} = req.body

      const t = await sequelize.transaction();
      const updateOrder = await Order.update({
        order_status: transaction_status
      },{
        where: {
          transaction_id
        },
        returning: true,
        transaction: t
      })

      const { user_id } = updateOrder[1][0]
      if (transaction_status) {
        const findTransaction = await Order.findOne({
          where: {
            transaction_id,
            user_id
          },
          include: [
            {
              model: Order_item
            }
          ]
        })
        if (transaction_status === 'settlement') {
          for (let i = 0; i < findTransaction.Order_items.length; i++) {
            const el = findTransaction.Order_items[i];
            await Books.increment('sold', {
              by: el.quantity,
              where: {
                id: el.book_id
              },
              transaction: t
            })
  
            await Cart.destroy({
              where: {
                book_id: el.book_id,
                user_id
              },
              transaction: t
            })
          }
        }
      }
      await t.commit()
      if (updateOrder) {
        console.log(updateOrder)
        res.json(updateOrder[1][0])
        req.app.get("SocketService").emitter('refreshPageAfterTransaction', ({ user_id }))
      } else {
        throw {name: 'internal_service_error'}
      }
    } catch (error) {
      await t.rollback()
      res.json(error)
    }
  }

  static async getAllOrders(req, res, next) {
    try {
      const { page, courier } = req.query
      let current_page = page ? page - 1 : 0
      let payload = {
        include: [
          { 
            model: Order_item,
            include: [{ model: Books }]
          }
        ],
        offset: current_page * 12,
        limit: 12
      }
      let where
      if (courier) {
        where = {
          courier_type: {
            [Op.iLike]: `%${courier}%`
          }
        }
        payload.where = where
      } 
      const total_order = await Order.count({where})
      const result = await Order.findAll(payload);
      const total_page = Math.ceil(total_order / 12)
      res.json({total_data: total_order, total_page, current_page, response: result});
    } catch (err) {
      next(err);
    }
  }

  static async getOrdersUser(req, res, next) {
    try {
      const { id } = req.logInUser
      const orderService = new OrderService()
      const data = await orderService.getOrdersByUserId(id)
      res.status(200).json(OrderController.getSuccessResponse(data, 'successfully get orders'))
    } catch (error) {
      res.json(error)
    }
  }

  static async getOrderById(req, res, next) {
    try {
      const { id } = req.params
      const orderService = new OrderService()
      const findTransaction = await orderService.getOrderById(id)
      res.status(200).json(OrderController.getSuccessResponse(findTransaction, 'success get order'))
    } catch (error) {
      next(error)
    }
  }
}

module.exports = OrderController;

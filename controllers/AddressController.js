const { User_Address } = require("../models");
const AddressService = require("../services/AddressService");

class AddressController {
  static getSuccessResponse (data = [], msg = '') {
		return {
		  status: 'success',
		  data: data,
		  msg: msg
		}
	}

  static async getAllUserAddress(req, res, next) {
    try {
      const addressService = new AddressService
      const result = await addressService.getAddressesByUserId(req.logInUser.id)
      res.status(200).json(AddressController.getSuccessResponse(result, 'success fetch addresses'))
    } catch (error) {
      next(error)
    }
  }

  static async create(req, res, next) {
    const { id } = req.logInUser
    const { province, city, postal_code, phone_number, receiver, detail_address, province_id, city_id, is_main_address } = req.body
    const addressService = new AddressService()
    try {
      if (is_main_address) {
        await addressService.updateAllAddress({ is_main_address: false }, id)
      }
      const createAddress = await addressService.createAddress({
        province, city, phone_number,
        receiver, postal_code, detail_address,
        province_id, city_id, user_id: id, is_main_address
      })
      res.status(201).json(AddressController.getSuccessResponse(createAddress, 'success create address'))
    } catch (error) {
      next(error)
    }
  }

  static async updateIsMainAddress(req, res, next) {
    try {
      const { id } = req.params
      const addressService = new AddressService()
      const findAddress = await addressService.findAddress(id)
      if (!findAddress) {
        throw { name: "not_found" }
      } else {
        await addressService.updateAllAddress({ is_main_address: false }, req.logInUser.id)
        const updateAddress = await addressService.updateSelectedAddress({ is_main_address: true }, id)
        res.status(200).json(AddressController.getSuccessResponse(updateAddress, 'successfully update address'))
      }
    } catch (error) {
      next(error)
    }
  }

  static async updateAddress(req, res, next) {
    try {
      const { id } = req.params
      const { province, city, postal_code, phone_number, receiver, detail_address, province_id, city_id, is_main_address } = req.body
      
      const addressService = new AddressService()
      const findAddress = await addressService.findAddress(id)
      if (!findAddress) {
        throw {name: 'data_not_found'}
      } else {
        if (is_main_address) {
          await addressService.updateSelectedAddress({ is_main_address: false }, id)
        }
        const updateAddress = await addressService.updateAddress(id, {
          province,
          city,
          phone_number,
          receiver,
          postal_code,
          detail_address,
          province_id,
          city_id,
          user_id: req.logInUser.id,
          is_main_address
        }, req.logInUser.id)
        res.status(200).json(AddressController.getSuccessResponse(updateAddress[1][0], 'success update address'))
      }
    } catch (error) {
      next(error)
    }
  }

  static async delete(req, res, next) {
    try {
      const { id } = req.params
      const findAddress = await User_Address.findOne({
        where: {
          id,
          user_id: req.logInUser.id
        }
      })
      if (!findAddress) {
        throw {name: 'not_found'}
      } else {
        const deleteAddress = await User_Address.destroy({
          where: {
            id,
            user_id: req.logInUser.id
          }
        })
        if (findAddress) {
          res.status(200).json('Address success deleted')
        } else {
          throw {name: 'internal_service_error'}
        }
      }
    } catch (error) {
      next(error)
    }
  }
}

module.exports = AddressController;

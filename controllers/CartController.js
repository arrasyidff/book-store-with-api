const { Cart } = require("../models");
const BookService = require("../services/BookService");
const CartService = require("../services/CartService");

class CartController {
  static async getSuccessResponse (data = [], msg = '') {
    return {
      status: 'success',
      data: data,
      msg: msg
    }
  }

  static async getAllCart(req, res, next) {
    try {
      const cartService = new CartService()
      if (!req.query.key) {
        const data = await cartService.getAllByUserId(req.logInUser.id)
        let copyData = JSON.parse(JSON.stringify(data));
        const result = copyData.map((el) => {
          if (el.is_selected === null) {
            el.is_selected = false;
          } else if (el.Book.stock === 0) {
            el.is_selected = false;
          }
          return el;
        });
        res.status(200).json(await CartController.getSuccessResponse(result, 'managed to get data'));
      } else if (req.query.key === "count") {
        const total_data = await Cart.count({
          where: {
            user_id: req.logInUser.id,
          },
        });
        res.status(200).json({total_data});
      }
    } catch (error) {
      next(error);
    }
  }

  static async addChart(req, res, next) {
    try {
      const { book_id } = req.params;
      const { id } = req.logInUser
      const { quantity } = req.body
      const findBook = await new BookService().getBookById(book_id);
      const cartService = new CartService()
      if (!findBook) {
        throw { name: "not_found" };
      }
      if (findBook.stock === 0) {
        throw {name: 'out_of_stock'}
      } else {
        if (quantity && quantity <= findBook.stock) {
          const findCart = await cartService.getCartByBookUserId(id, book_id)
          const cartId = findCart ? findCart.id : null
          const upsertCart = await cartService.findAndCreate(id, cartId, book_id, quantity)
          res.status(200).json(await CartController.getSuccessResponse(upsertCart[0], "cart added"))
        } else {
          throw { name: "maximum_stock" };
        }
      }
    } catch (err) {
      next(err);
    }
  }

  static async increment(req, res, next) {
    try {
      const { book_id } = req.params;
      const { id } = req.logInUser 
      const findBook = await new BookService().getBookById(book_id);
      if (!findBook) {
        throw { name: "not_found" };
      }

      const cartService = await new CartService();
      const findItemCart = await cartService.getCartByBookUserId(id, book_id);
      if (findItemCart) {
        if (findItemCart.quantity < findBook.stock) {
          const incrementQuantityItem = await cartService.increment('quantity', book_id, id)
          res.status(200).json(await CartController.getSuccessResponse(incrementQuantityItem[0][0][0], 'quantity added successfully'));
        } else {
          throw { name: "maximum_stock" };
        }
      } else {
        throw { name: "not_found" };
      }
    } catch (err) {
      next(err);
    }
  }

  static async decrement(req, res, next) {
    try {
      const { book_id } = req.params;
      const { id } = req.logInUser

      const findBook = await new BookService().getBookById(book_id);
      if (!findBook) {
        throw { name: "not_found" };
      }
      const cartService = new CartService()
      const findItemCart = await cartService.getCartByBookUserId(id, book_id)
      if (findItemCart) {
        if (findItemCart.quantity > 0) {
          const decrementQuantityItem = await cartService.decrement('quantity', book_id, id)
          res.status(200).json(await CartController.getSuccessResponse(decrementQuantityItem[0][0][0], "quantity successfully deducted"));
        } else {
          throw { name: "minimum_quantity" };
        }
      } else {
        throw { name: "not_found" };
      }
    } catch (err) {
      next(err);
    }
  }

  static async updateSelected(req, res, next) {
    try {
      const { book_id } = req.params;
      const { is_selected } = req.body;
      const { key } = req.query;
      const { id } = req.logInUser

      const cartService = new CartService()
      if (!key) {
        const findBook = await new BookService().getBookById(book_id)
        if (!findBook) {
          throw {name: 'not_found'}
        }
        const findCart = await cartService.getCartByBookUserId(id, book_id)
        if (!findCart) {
          throw {name: 'not_foud_cart'}
        }
        const updateCart = await cartService.updateItem({ is_selected: !findCart.is_selected }, book_id, id)
        res.status(200).json(await CartController.getSuccessResponse(updateCart[1][0], 'selected has been updated'))
      } else if (key) {
        if (key === 'all') {
          const updateIsSelectedCartUser = await cartService.updateSelectedAllItem(is_selected, id)
          res.json(await CartController.getSuccessResponse(updateIsSelectedCartUser[1], 'all selected has been updated'))
        }
      }
    } catch (error) {
      next(error);
    }
  }

  static async delete(req, res, next) {
    try {
      const { book_id } = req.params;
      const deleteCart = await new CartService().deleteItem(book_id, req.logInUser.id)
      if (!deleteCart) {
        throw { name: "not_found" };
      }
      res.status(200).json(await CartController.getSuccessResponse(1, "successfully delete item"));
    } catch (err) {
      next(err);
    }
  }

  static async deleteItems(req, res, next) {
    try {
      const { booksId } = req.body;
      const cartService = new CartService()
      if (booksId.length > 0) {
        const deleteCarts = await cartService.deleteItems(booksId, req.logInUser.id)
        res.status(200).json(await CartController.getSuccessResponse(deleteCarts, 'success delete all items'));
      } else {
        throw { name: "not_found_1_or_all_book" };
      }
    } catch (error) {
      next(error);
    }
  }
}

module.exports = CartController;

const { Shipment, Books, Users, User_Address, sequelize } = require("../models")
const AddressService = require("../services/AddressService")
const BookService = require("../services/BookService")
const ShipmentService = require("../services/ShipmentService")

class ShipmentController {
	static getSuccessResponse (data = [], msg = '') {
		return {
		  status: 'success',
		  data: data,
		  msg: msg
		}
	}
	  
	static async getAllShipment(req, res, next) {
		try {
			const shipmentService = new ShipmentService()
			const result = await shipmentService.getAllShipmentsByUserId(req.logInUser.id)
			res.status(200).json(ShipmentController.getSuccessResponse(result, 'success fetch shipments'))
		} catch (error) {
			next(error)
		}
	}

	static async create(req, res, next) {
		try {
			const { items } = req.body
			const { id } = req.logInUser
			const books = items.map(el => {
				return Number(el.book_id)
			})

			const bookService = new BookService()
			const findBooks = await bookService.getBooksByIds(books)
			if (findBooks.books.length !== books.length) {
				throw {name: 'not_found_1_or_all_book'}
			}

			findBooks.books.forEach(((el, index) => {
				if (el.stock === 0) {
					throw {name: 'out_of_stock'}
				} else if (el.stock <  items[index].quantity) {
					throw {name: 'maximum_stock'}
				}
			}))
			
			const shipmentService = new ShipmentService()
			const addressService = new AddressService()
			const findAddresses = await addressService.getAddressesByUserId(id)
			if (findAddresses.length === 0) {
				throw {name: 'not_found_address'}
			} else {
				const findUserShipment = await shipmentService.getAllShipmentsByUserId(id)
				if (findUserShipment.length > 0) {
					const createShipments = await shipmentService.createShipments(items, id, true)
					res.status(201).json(ShipmentController.getSuccessResponse(createShipments, 'success create'))
					
				} else {
					const createNewShipments = await shipmentService.createShipments(items, id, false)
					res.status(201).json(ShipmentController.getSuccessResponse(createNewShipments, 'success create'))
				}
			}
		} catch (error) {
			next(error)
		}
	}
}
module.exports = ShipmentController
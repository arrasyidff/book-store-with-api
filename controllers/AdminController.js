const { Admin } = require("../models")
const { comparePass } = require("../helper/generatePassword")
const { generateToken } = require("../helper/generateToken")

class AdminController {
    static async register(req, res, next) {
        try {
            const {email, password, role} = req.body
            let createAdmin = await Admin.create({email, password, role})
            delete createAdmin['password']
            res.status(201).json({response: {
                email: createAdmin.email
            }})
        } catch (err) {
            next(err)
        }
    }

    static async login(req, res, next) {
        try {
            const { email, password } = req.body
            const findAdmin = await Admin.findOne({where: { email }})
            if (!findAdmin) {
                next({name: "invalid_account"})
            } else {
                if (comparePass(password, findAdmin.password)) {
                    const access_token = generateToken({id: findAdmin.id, email: findAdmin.email, role: findAdmin.role})
                    res.status(200).json({access_token})
                } else {
                    next({ name: "invalid_account" })
                }
            }
        } catch (err) {
            next(err)
        }
    }
}

module.exports = AdminController
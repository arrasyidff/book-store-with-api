const request = require('supertest')
const app = require('../app')
const { sequelize } = require('../models')
const { queryInterface } = sequelize

describe('GET book', () => {
    describe('Get name books', () => {
        it('success get books name', (done) => {
            request(app)
                .get('/book-name?title=b')
                .end((err, res) => {
                    const { body, status } = res
                    if (err) return done(err)
                    expect(status).toBe(200)
                    expect(body).toHaveProperty('status', 'success')
                    expect(body).toHaveProperty('data', expect.any(Array))
                    expect(body).toHaveProperty('msg', 'managed to get data')
                    done()
                })
        })

        it('success get all books', (done) => {
            const query = {
                limit: 10,
                page: 1
            }
            request(app)
                .get('/books')
                .query(query)
                .end((err, res) => {
                    const { body, status,  } = res
                    if (err) return done(err)
                    expect(status).toBe(200)
                    expect(body).toHaveProperty('status', 'success')
                    expect(body).toHaveProperty('data', expect.any(Object))
                    expect(body).toHaveProperty('data.total_data', expect.any(Number))
                    expect(body).toHaveProperty('data.total_page', expect.any(Number))
                    expect(body.data.total_page).toEqual(Math.ceil(body.data.total_data / query.limit))
                    expect(body).toHaveProperty('data.current_page', expect.any(Number))
                    expect(body.data.current_page).toEqual(query.page)
                    expect(body).toHaveProperty('data.data', expect.any(Array))
                    expect(body).toHaveProperty('msg', 'managed to get data')
                    done()
                })
        })
    })

    describe('Get book by id', () => {
        it('success get book by id', (done) => {
            request(app)
                .get('/book/1')
                .end((err, res) => {
                    const { body, status } = res
                    if (err) return done(err)
                    expect(status).toBe(200)
                    expect(body).toHaveProperty('status', 'success')
                    expect(body).toEqual(body)
                    done()
                })
        })

        it('success get book by id with query user_id', (done) => {
            request(app)
                .get('/book/13?user_id=1')
                .end((err, res) => {
                    const { body, status } = res
                    if (err) return done(err)
                    expect(status).toBe(200)
                    expect(body).toHaveProperty('status', 'success')
                    expect(body).toHaveProperty('data', expect.any(Object))
                    expect(body.data.Cart).toBeDefined()
                    expect(body).toHaveProperty('data.is_wishlist', expect.any(Boolean))
                    expect(body).toEqual(body)
                    done()
                })
        })

        it('not found get book by id', (done) => {
            request(app)
                .get('/book/0')
                .end((err, res) => {
                    const { body, status } = res
                    if (err) return done(err)
                    expect(status).toBe(404)
                    expect(body).toHaveProperty('msg', 'data not found')
                    expect(body).toEqual(body)
                    done()
                })
        })
    })
})
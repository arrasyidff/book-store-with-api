const { Shipment, Books, Users, User_Address, sequelize } = require("../models")
const { Op } = require("sequelize")

class ShipmentService {
    async getAllShipmentsByUserId (userId) {
        try {
            return await Shipment.findAll({
                where: {
                    user_id: userId
                },
                include: [
                    { model: Books },
                    { model: Users }
                ]
            })
        } catch (error) {
            throw error            
        }
    }

    async createShipments (items, userId, isAnyItems) {
        try {
            const t = await sequelize.transaction()
            if (isAnyItems) {
                await Shipment.destroy({
                    where: {
                        user_id: userId
                    }
                }, { transaction: t })
            }
    
            for (let i = 0; i < items.length; i++) {
                const item = items[i]
                await Shipment.create({
                    book_id: item.book_id,
                    user_id: userId,
                    quantity: item.quantity
                }, { transaction: t })  
            }
    
            await t.commit()
            return await 'success create shipments'
        } catch (error) {
            await t.rollback()
            throw error
        }
    }
}

module.exports = ShipmentService
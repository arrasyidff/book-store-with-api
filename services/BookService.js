const { Books, Publisher, Author, Category, Cart } = require('../models')
const { Op } = require("sequelize");

class BookService {
  async getAllBooks (query, limit, offset) {
    try {
      return await this.lazyLoadBooks(query, limit, offset)
    } catch (error) {
      throw error      
    }
  }

  async getBooksByCategory (query, limit, offset) {
    try {
      query = {category: query} 
      return await this.lazyLoadBooks(query, limit, offset)
    } catch (error) {
      throw error      
    }
  }

  async getBooksByAuthor (query, limit, offset) {
    try {
      query = {author: query} 
      return await this.lazyLoadBooks(query, limit, offset)
    } catch (error) {
      throw error      
    }
  }

  async getBooksByPublisher (query, limit, offset) {
    try {
      query = {publisher: query} 
      return await this.lazyLoadBooks(query, limit, offset)
    } catch (error) {
      throw error      
    }
  }

  async getBooksByTitle (query, limit, offset) {
    try {
      query = {title: query} 
      return await this.lazyLoadBooks(query, limit, offset)
    } catch (error) {
      throw error     
    }
  }

  async getBooksByIds (query, limit, offset) {
    try {
      query = {ids: query} 
      return await this.lazyLoadBooks(query, limit, offset)
    } catch (error) {
      throw error      
    }
  }

  async lazyLoadBooks (query, limit = null, offset) {
    try {
      let option = {
        attributes: [
          "id",
          "title",
          "isbn_number",
          "date_of_publish",
          "number_of_pages",
          "price",
          "cover",
          "stock",
        ],
        where: {
          stock: {
            [Op.gt]: 0,
          },
        },
        include: [
          {
            key: 'category',
            model: Category,
            attributes: ["name"]
          },
          {
            key: 'author',
            model: Author,
            attributes: ["full_name"]
          },
          {
            key: 'publisher',
            model: Publisher,
            attributes: ["name"]
          }
        ],
        offset,
        order: [['createdAt', 'ASC']]
      }
  
      let optionCount = {
        where: option.where
      }
  
      if (query) {
        Object.keys(query)
          .forEach(key => {
            if (key === 'category') {
              const whereInclude = { name: query.category }
              optionCount.include = [
                {
                  model: Category,
                  where: whereInclude
                }
              ]
              option.include = option.include.map(model => {
                if (model.key == 'category') model.where = whereInclude
                return model
              })
            } else if (key === 'author') {
              const whereInclude = { full_name: query.author }
              optionCount.include = [
                {
                  model: Author,
                  where: whereInclude
                }
              ]
              option.include = option.include.map(model => {
                if (model.key == 'author') model.where = whereInclude
                return model
              })
            } else if (key === 'publisher') {
              const whereInclude = { name: query.publisher }
              optionCount.include = [
                {
                  model: Publisher,
                  where: whereInclude
                }
              ]
              option.include = option.include.map(model => {
                if (model.key == 'publisher') model.where = whereInclude
                return model
              })
            } else if (key === 'title') {
              option.where.title = {[Op.iLike]: `%${query.title}%`}
            } else if (key === 'ids') {
              option.where.id = query.ids
            }
          });
      }
  
      if (limit) option['limit'] = limit
      const total = await Books.count(optionCount)
      const books = await Books.findAll(option);
      const pages = Math.ceil(total/limit)
      return { total, books, pages }
    } catch (error) {
      throw error      
    }
  }

  async getNamesBook (title, limit = 5) {
    try {
      return await Books.findAll({
        attributes: ["id", "title"],
        where: {
          title: {
            [Op.iLike]: `%${title}%`
          }
        },
        limit
      })
    } catch (error) {
      throw error      
    }
  }

  async getBookById (bookId) {
    try {
      const option = {
        where: { id: bookId },
        include: [
          {
            model: Publisher,
            attributes: ["name"]
          },
          {
            model: Author,
            attributes: ["full_name"]
          },
          {
            model: Category,
            attributes: ["name"]
          }
        ],
      }

      return await Books.findOne(option)
    } catch (error) {
      throw error
    }
  }
}

module.exports = BookService
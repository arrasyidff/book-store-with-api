const { Cart, Books, Order, Order_item, Shipment, Users, sequelize, User_Address } = require("../models");
const { Op } = require("sequelize")
const Axios = require('axios');

class OrderService {
    async getOrdersByUserId (userId) {
        try {
            return Order.findAll({
                where: { user_id: userId },
                include: [{ model: Order_item, include: [{ model: Books }] }],
                order: [['createdAt', 'DESC']]
            })
        } catch (error) {
            throw error            
        }
    }

    async getOrderById (orderId) {
        try {
            const order = await Order.findOne({
                where: { id: orderId },
                include: [
                    {
                        model: Order_item,
                        include: [{ model: Books }]
                    },
                    { model: User_Address }
                ]
            })
            if (!order) {
                throw { name: 'not_found' }
            }
            return order
        } catch (error) {
            throw error
        }
    }

    async checkout (userId, payment_method, total_price, courier, cost, user_address_id, books) {
        const t = await sequelize.transaction()
        try {
            const createOrder = await Order.create({
                    user_id: userId,
                    total_price: total_price,
                    courier_type: courier,
                    cost: cost,
                    user_address_id: user_address_id,
                    payment_method
                }, { transaction: t }
            )

            for (let i = 0; i < books.length; i++) {
                const item = books[i];
                await Order_item.create({
                    order_id: createOrder.id,
                    book_id: item.book_id,
                    quantity: item.quantity
                }, { transaction: t })
            }

            const findOerderItemUser = await Order_item.findAll({
                where: { order_id: createOrder.id },
                include: [{ model: Books }],
                transaction: t
            })

            const findUser = await Users.findOne({
                where: { id: userId },
                attributes: { exclude: ["password"] },
                transaction: t
            })
    
            const splitUserName = findUser.full_name.split(' ')
            let item_details = findOerderItemUser.map(item => {
                return {
                    id: item.book_id,
                    name: item.Book.title,
                    price: item.Book.price,
                    quantity: item.quantity
                }
            })
    
            const date = new Date(new Date().getTime() + 60 * 60 * 24 * 1000)
            const month = date.getMonth() < 9 ? `0${date.getMonth() + 1}` : date.getMonth() + 1
            const day = date.getDate() < 9 ? `0${date.getDate()}` : date.getDate()
            const year = `${date.getFullYear()}-${month}-${day}`
            const time = date.toString().split(' ')[4]
            const gmt = '+' + date.toString().split(' ')[5].split('+')[1]
            const expireDate = `${year} ${time} ${gmt}`
            const midtransPayload = {
                payment_type: 'echannel',
                transaction_details: {
                    order_id: createOrder.id,
                    gross_amount: total_price
                },
                customer_details: {
                    email: findUser.email,
                    first_name: splitUserName[0],
                    last_name: splitUserName[1],
                    phone: findUser.phone_number
                },
                item_details,
                echannel: {
                    bill_info1: 'Payment of BukuBooks',
                    bill_info2: 'debt'
                },
                // custom_expiry: {
                //   order_time: expireDate,
                //   expiry_duration: 60,
                //   unit: "second"
                // }
            }
            const payMidtrans = await Axios({
                url: 'https://api.sandbox.midtrans.com/v2/charge',
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic U0ItTWlkLXNlcnZlci02T2tZMjFIcGNHenA1dUZxRHVkNHlIRjQ6'
                },
                data: midtransPayload
            })

            console.log(payMidtrans)

            if (!payMidtrans.data.transaction_id) {
                throw {name: 'error midtrans'}
            }

            const updateOrder = await Order.update(
                {
                    invoice: 'BB-' + payMidtrans.data.order_id + '-' + Date.now(),
                    transaction_id: payMidtrans.data.transaction_id,
                    order_status: payMidtrans.data.transaction_status,
                    bill_key: payMidtrans.data.bill_key,
                    biller_code: payMidtrans.data.biller_code
                },
                {
                    where: { id: createOrder.id },
                    returning: true,
                    transaction: t
                }
            )

            await Shipment.destroy({
                where: { user_id: userId },
                transaction: t
            })

            await t.commit()
            return updateOrder
        } catch (error) {
            await t.rollback()
            throw error   
        }
    }
}

module.exports = OrderService
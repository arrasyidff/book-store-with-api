const { Cart, Books } = require("../models")

class CartService {
  async getAllByUserId (userId) {
    try {
      return await Cart.findAll({
        where: {
          user_id: userId
        },
        include: [
          {
            model: Books
          }
        ],
        order: [['createdAt', 'DESC']]
      })
    } catch (error) {
      throw error      
    }
  }

  async getCartByBookUserId (userId, bookId) {
    try {
      return await Cart.findOne({
        where: {
          user_id: +userId,
          book_id: bookId
        }
      })
    } catch (error) {
      throw error
    }
  }

  async findAndCreate (userId, cartId, bookId, quantity = 1) {
    try {
      let option = {
        book_id: bookId,
        quantity: quantity,
        user_id: userId,
        is_selected: false
      }
      if (cartId) option.id = cartId
      return await Cart.upsert(option)
    } catch (error) {
      throw error
    }
  }

  async increment (field, bookId, userId) {
    try {
      return await Cart.increment(field, {
        where: {
          book_id: bookId,
          user_id: userId
        }
      })
    } catch (error) {
      throw error      
    }
  }

  async decrement (field, bookId, userId) {
    try {
      return await Cart.decrement(field, {
        where: {
          book_id: bookId,
          user_id: userId
        }
      })
    } catch (error) {
      throw error      
    }
  }

  async deleteItem (bookId, userId) {
    try {
      return await Cart.destroy({
        where: {
          book_id: bookId,
          user_id: userId
        }
      })
    } catch (error) {
      throw error      
    }
  }

  async deleteItems (booksId, userId) {
    try {
      return await Cart.destroy({
        where: {
          book_id: booksId,
          user_id: userId
        }
      })
    } catch (error) {
      throw error      
    }
  }

  async updateItem (payload, bookId, userId) {
    try {
      return await Cart.update(payload, {
        where: {
          user_id: userId,
          book_id: bookId
        },
        returning: true
      })
    } catch (error) {
      throw error      
    }
  }

  async updateSelectedAllItem (payload, userId) {
    try {
      return await Cart.update({is_selected: payload}, {
        where: {
          user_id: userId
        },
        returning: true
      })
    } catch (error) {
      throw error      
    }
  }
}

module.exports = CartService
const { Wishlist, Books, Users, Author } = require("../models")

class WishlistService {
  async getWishlistByBookId (userId, bookId) {
    try {
      return await Wishlist.findOne({
        where: {
          book_id: bookId,
          user_id: userId
        }
      })
    } catch (error) {
      throw error      
    }
  }

  async getWishlistByBookUserId (userId, booksId) {
    try {
      return await Wishlist.findAll({
        where: {
          user_id: +userId,
          book_id: booksId
        }
      })
    } catch (error) {
      throw error      
    }
  }

  async getTotalWishlist (userId) {
    try {
      return await Wishlist.count({
        where: {
          user_id: userId
        }
      })
    } catch (error) {
      throw error      
    }
  }
  
  async getAllWishlist (userId) {
    try {
      const result = await Wishlist.findAll({
        attributes: { exclude: ["createdAt", "updatedAt"] },
        include: [
          {
            model: Books,
            attributes: { exclude: ["createdAt", "updatedAt"] },
            include: [
              {
                model: Author
              }
            ]
          },
          {
            model: Users,
            attributes: { exclude: ["createdAt", "updatedAt"] },
          },
        ],
        where: {
          user_id: userId
        }
      })
  
      return result
    } catch (error) {
      throw error
    }
  }

  async addWishlist (userId, bookId) {
    try {
      return await Wishlist.create({ book_id: bookId, user_id: userId })
    } catch (error) {
      throw error
    }
  }

  async deleteWishlist (userId, bookId) {
    try {
      return await Wishlist.destroy({
        where: {
          user_id: userId,
          book_id: bookId
        }
      })
    } catch (error) {
      throw error      
    }
  }
}

module.exports = WishlistService
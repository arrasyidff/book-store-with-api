const { Cart, Books, Order, Order_item, Shipment, Users, sequelize, User_Address } = require("../models");
const { Op } = require("sequelize")

class OrderItemService {
    async findAllOrderItemByOrderId (orderId) {
        try {
            return await Order_item.findAll({
                where: { order_id: orderId }
            })
        } catch (error) {
            throw error
        }
    }
}

module.exports = OrderItemService
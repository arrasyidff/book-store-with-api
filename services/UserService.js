const { Users, User_Address } = require("../models");

class UserService {
    async getUser (userId) {
      try {
        return await Users.findOne({
          attributes: { exclude: ["password", "createdAt", "updatedAt"] },
          where: { id: userId },
          include: [
            {
              model: User_Address,
              attributes: { exclude: ["createdAt", "updatedAt"] },
            },
          ],
        });
      } catch (error) {
        throw error
      }
    }

    async getUserByEmail (email) {
      try {
        return await Users.findOne({
          attributes: { exclude: ["password", "createdAt", "updatedAt"] },
          where: { email },
          include: [
            {
              model: User_Address,
              attributes: { exclude: ["createdAt", "updatedAt"] },
            },
          ],
        });
      } catch (error) {
        throw error
      }
    }

    async update (userId, { full_name, date_of_birth, gender, email, phone_number }) {
      try {
        return await Users.update(
          { full_name, date_of_birth, gender, email, phone_number },
          {
            where: {
              id: userId
            },
            returning: true
          }
        )
      } catch (error) {
        throw error
      }
    }
}

module.exports = UserService
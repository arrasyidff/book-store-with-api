const { Shipment, Books, Users, User_Address, sequelize } = require("../models")

class AddressService {
    async getAddressesByUserId (userId) {
        try {
            return await User_Address.findAll({
                where: {
                    user_id: userId
                },
                order: [['updatedAt', 'DESC']]
            })
        } catch (error) {
            throw error            
        }
    }

    async findAddress (addressId) {
        try {
            return await User_Address.findOne({
                where: {
                    id: addressId
                }
            })
        } catch (error) {
            throw error            
        }
    }

    async createAddress ({
        province, city, phone_number,
        receiver, postal_code, detail_address,
        province_id, city_id, user_id, is_main_address
    }) {
        try {
            return await User_Address.create({
                province, city, phone_number,
                receiver, postal_code, detail_address,
                province_id, city_id, user_id, is_main_address
            })
        } catch (error) {
            throw error            
        }
    }

    async updateSelectedAddress (payload, addressId) {
        try {
            return await User_Address.update(payload, {
                where: {
                    id: addressId
                },
                returning: true
            })
        } catch (error) {
            throw error            
        }
    }

    async updateAllAddress (payload, userId) {
        try {
            return await User_Address.update(payload, {
                where: {
                    user_id: userId
                }
            })
        } catch (error) {
            throw error            
        }
    }

    async updateAddress (addressId, payload, userId) {
        try {
            return await User_Address.update(payload,
              {
                where: {
                  id: addressId,
                  user_id: userId
                },
                returning: true
              })
        } catch (error) {
            throw error
        }
    }
}

module.exports = AddressService
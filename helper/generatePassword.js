const bcrypt = require("bcrypt")

const hashingPass = (userPass) => {
    const salt = bcrypt.genSaltSync(10)
    return bcrypt.hashSync(userPass, salt)
}

const comparePass = (userPass, hashPass) => {
    return bcrypt.compareSync(userPass, hashPass)
}

module.exports = {
    hashingPass,
    comparePass
}
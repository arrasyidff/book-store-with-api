const multer = require("multer");
const fs = require("fs");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    let destination = file.fieldname
    if (req.body.type && req.params.id) {
      destination = `${req.body.type}/${req.params.id}`
    }
    if (file.fieldname === 'cover') {
      destination = `${destination}/${req.params.id}`
    }
    const path = `./uploads/${destination}`;
    fs.mkdirSync(path, { recursive: true }); // make can sub folder
    cb(null, path);
  },
  filename: (req, file, cb) => {
    let fileName = 'user-' + new Date().toISOString() + file.originalname
    if (file.originalname.includes('user-')) {
      cb(null, file.originalname);
    } else {
      cb(null, fileName);
    }
  },
});
const fileFilter = (req, file, cb) => {
  // filter extension
  if (file.mimetype === "image/png" || file.mimetype === "image/jpeg" || file.mimetype === "image/jpg") {
    cb(null, true);
  } else {
    cb({ message: "extension must be png|jpeg|jpg" }, false);
  }
};

module.exports = {
  storage,
  fileFilter
};
